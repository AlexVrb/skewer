/*
    This file is part of Skewer.

    Skewer is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Skewer is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Skewer.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>

#include <boost/program_options.hpp>

#include "SkewerPlayer.h"

const std::string c_helpKey = "help";
const std::string c_versionKey = "version";

void print_version()
{
    std::cout << "Skewer " << VERSION_MAJOR << '.' << VERSION_MINOR << '.'
              << VERSION_PATCH << '\n';
}

int main(int argc, char** argv)
{
    boost::program_options::options_description desc{"Options"};
    desc.add_options()(c_helpKey.c_str(), "Show help screen")(
        c_versionKey.c_str(), "Show version info");

    boost::program_options::variables_map varMap;
    store(parse_command_line(argc, argv, desc), varMap);

    if(varMap.count(c_helpKey)) {
        std::cout << desc;
        return 0;
    }
    if(varMap.count(c_versionKey)) {
        print_version();
        return 0;
    }

    return 0;
}
