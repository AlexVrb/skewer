set(PLAYER_SOURCEDIR src/${PLAYER_NAME})

set(PLAYER_SOURCES
    AudioPlayer.cpp
    main.cpp
    SDLRenderer.cpp
    SkewerPlayer.cpp
)

add_executable(${PLAYER_NAME} ${PLAYER_SOURCES})
include_directories(../../include)

add_definitions(-DVERSION_MAJOR=${SKEWER_VERSION_MAJOR}
                -DVERSION_MINOR=${SKEWER_VERSION_MINOR}
                -DVERSION_PATCH=${SKEWER_VERSION_PATCH}
)

find_package(Boost REQUIRED COMPONENTS program_options)
if(Boost_FOUND)
  include_directories(${Boost_INCLUDE_DIRS})
  target_link_libraries(${PLAYER_NAME} ${Boost_LIBRARIES})
endif()

find_package(SDL2 REQUIRED)
if(SDL2_FOUND)
  include_directories(${SDL2_INCLUDE_DIRS})
  target_link_libraries(${PLAYER_NAME} ${SDL2_LIBRARIES})
endif()

target_link_libraries(${PLAYER_NAME} ${LIB_NAME})
