/*
    This file is part of Skewer.

    Skewer is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Skewer is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Skewer.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>

#include <yaml-cpp/yaml.h>

#include "FrameFactory.h"

#include "SkewerGenericFrame.h"

const std::string c_yamlExtension = ".yaml";

// common node names
const std::string c_typeNode = "type";
const std::string c_textNode = "text";
const std::string c_actionsNode = "actions";

// map string types to enum
const std::map<std::string, FrameType> string2type{{"generic", GENERIC_FRAME},
                                                   {"menu", MENU},
                                                   {"video", VIDEO}};

FrameFactory::FrameFactory(const std::string& gameName) : m_gameName(gameName)
{
}

int FrameFactory::loadFrame(const std::string& frameName,
                            SkewerFrame* loadedFrame) const
{
    std::string fileName = m_gameName + frameName + c_yamlExtension;
    YAML::Node frameFile = YAML::LoadFile(fileName);

    if(frameFile.IsNull()) {
        std::cerr << "File " << fileName << " doesn't exist!\n";
        return -1;
    }

    if(frameFile[c_typeNode]) {
        std::string frameType = frameFile[c_typeNode].as<std::string>();
        loadedFrame = createFrameByType(frameType);

        if(!loadedFrame) {
            std::cerr << "Frame cannot be created!\n";
            return -1;
        }
    }
    else {
        std::cerr << "Type node doesn't exist in file " << fileName << '\n';
        return -1;
    }

    return 0;
}

SkewerFrame*
FrameFactory::createFrameByType(const std::string& strFrameType) const
{
    SkewerFrame* frame = nullptr;
    // map string type to enumerated equivalent
    auto itFrameType = string2type.find(strFrameType);
    if(itFrameType == string2type.end()) {
        std::cerr << "String type " << strFrameType << " is not recognized.\n";
        return nullptr;
    }

    FrameType enumFrameType = itFrameType->second;
    switch(enumFrameType) {
        case GENERIC_FRAME: {
        }

        case MENU:
        case VIDEO:
        default: {
            std::cerr << "Frame type not recognized or not implemented yet.\n";
            frame = nullptr;
        }
    }

    return frame;
}
