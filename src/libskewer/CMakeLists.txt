set(LIB_SOURCEDIR src/lib${LIB_NAME})

set(LIB_SOURCES
    FrameFactory.cpp
    SkewerFrame.cpp
    SkewerGenericFrame.cpp
)

add_library(${LIB_NAME} SHARED ${LIB_SOURCES})
include_directories(../../include)

set(SOVER ${SKEWER_VERSION_MAJOR}.${SKEWER_VERSION_MINOR}.${SKEWER_VERSION_PATCH})

set_target_properties(${LIB_NAME} PROPERTIES
  VERSION ${SOVER}
  SOVERSION ${SOVER})

find_package(yaml-cpp REQUIRED)
if (yaml-cpp_FOUND)
  include_directories(${YAML_CPP_INCLUDE_DIR})
  target_link_libraries(${LIB_NAME} ${YAML_CPP_LIBRARIES})
endif()
