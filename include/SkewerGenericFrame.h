/*
    This file is part of Skewer.

    Skewer is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Skewer is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Skewer.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SKEWERGENERICFRAME_H
#define SKEWERGENERICFRAME_H

#include <vector>

#include "SkewerFrame.h"

class SkewerGenericFrame : public SkewerFrame {
  public:
    SkewerGenericFrame(const FrameType);

  protected:
    std::string m_backgroundFileName;
    std::vector<std::string> m_spritesFileNames;
    std::string m_text;
};

#endif
