/*
    This file is part of Skewer.

    Skewer is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Skewer is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Skewer.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FRAMEFACTORY_H
#define FRAMEFACTORY_H

#include <string>

class SkewerFrame;

class FrameFactory {
  public:
    FrameFactory(const std::string& gameName);

    int loadFrame(const std::string& frameName, SkewerFrame* loadedFrame) const;

  private:
    SkewerFrame* createFrameByType(const std::string& frameType) const;

    std::string m_gameName;
};

#endif
